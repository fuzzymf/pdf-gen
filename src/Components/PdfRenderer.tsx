// import React from 'react';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';

// Create styles
const styles = StyleSheet.create({
	container: {
		width: '100%',
		height: '100vh', // Set the height of the container to the full viewport height
	},
	page: {
		flexDirection: 'row',

		width: '100%',
		height: '100%', // Set the height of the page to fill the container
	},
	section: {
		margin: 10,
		padding: 10,
		flexGrow: 1,
	},
});
// Create Document Component
const PdfRenderer = (props: {
	name: string;
	age: number;
	address: string;
	city: string;
	state: string;
	zip: string;
}) => (
	<Document>
		<Page size="A4" style={styles.page}>
			<View style={styles.section}>
				<Text>
					{props.name}
				</Text>
			</View>
		</Page>
	</Document>
);

export default PdfRenderer;