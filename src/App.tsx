import React from 'react';
import { PDFViewer } from '@react-pdf/renderer';
import Divider from '@mui/material/Divider';
import './App.css';
import PdfGenerator from './Components/PdfGenerator';
import PdfRenderer from './Components/PdfRenderer';

export const App: React.FC = () => {
	const [name, setName] = React.useState<string>('');
	const [age, setAge] = React.useState<number>(0);
	const [address, setAddress] = React.useState<string>('');
	const [city, setCity] = React.useState<string>('');
	const [state, setState] = React.useState<string>('');
	const [zip, setZip] = React.useState<string>('');
	const [rows, setRows] = React.useState<any>([]);

	return (
		<>
			<div className='App'>
				<PdfGenerator
					name={name}
					age={age}
					address={address}
					city={city}
					state={state}
					zip={zip}
					setName={setName}
					setAge={setAge}
					setAddress={setAddress}
					setCity={setCity}
					setState={setState}
					setZip={setZip}
					setRows={setRows}
				/>
				<Divider orientation="vertical" flexItem />
				<PDFViewer className="PDFViewer-root">
					<PdfRenderer
						name={name}
						age={age}
						address={address}
						city={city}
						state={state}
						zip={zip}
						setName={setName}
						setAge={setAge}
						setAddress={setAddress}
						setCity={setCity}
						setState={setState}
						setZip={setZip}
						rows={rows}
					/>
				</PDFViewer>
			</div>
		</>
	);
};

export default App;
